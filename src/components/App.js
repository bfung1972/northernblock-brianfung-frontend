import React from 'react';
import Header from './Header';
import Footer from './Footer';
import './App.css';
import DogGrid from './DogGrid';

const App = () => {
  return (
    <div className="App">
      <Header />
      <main className="App__main">
        <DogGrid />
      </main>
      <Footer />
    </div>
  );
};

export default App;
