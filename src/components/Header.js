import React from 'react';
import Wrapper from './Wrapper';
import './Header.css';

const Header = () => {
  return (
    <header className="Header">
      <Wrapper>
        <div className="Header__content">
          <h1 className="Header__title">CryptoDogs</h1>
          <p>The best dogs, brought to you on Web 3.0</p>
        </div>
      </Wrapper>
    </header>
  );
};

export default Header;
