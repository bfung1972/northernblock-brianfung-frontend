import React from 'react';
import love from '../assets/like.svg';
import Wrapper from './Wrapper';
import './Footer.css';

const Footer = () => {
  return (
    <footer className="Footer">
      <Wrapper>
        <p className="Footer__text">
          Made with <img src={love} alt="love" /> by Northern Block
        </p>
      </Wrapper>
    </footer>
  );
};

export default Footer;
