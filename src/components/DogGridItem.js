import React,{Component} from 'react';
import {Media,Grid,Col,Row} from 'react-bootstrap';
import './DogGridItem.css';
import Favorited from './Favorited';

class DogGridItem extends Component{
    count = 0;
    
    constructor(params){
        super(params);
        this.capitalCase = this.capitalCase.bind(this);
    }

    

    render(){
        return(
            <Media>
                <Media.Left>
                    <img src={this.props.dog.image} alt={this.props.dog.name} className="dogImage" />
                    
                </Media.Left>
                <Media.Body>
                    <Media.Heading>{this.props.dog.sex === "male" ? this.props.dog.name + " (neutered)" : this.props.dog.name + " (sprayed)" }<Favorited dogName={this.props.dog.name} dogId={this.props.dog.id} isFavorited={this.props.dog.favorite} click={this.props.favoriteClick} /></Media.Heading>
                    <Grid>
                    
                        {Object.keys(this.props.dog).map(
                            (key) => {
                                let ignore = new Set(["name","spayedOrNeutered","id","image","favorite"]);
                                
                                return(!ignore.has(key) ?
                                <Row className="show-grid">
                                        <Col xs={3} sm={1}>{key === "priceInEth" ? "Price:" : this.capitalCase(key) + ":"}</Col>
                                        <Col xs={9} sm={10}>{this.props.dog[key]}</Col>
                                </Row>    
                                : null);  
                            }
                        )}
                       
                    </Grid>
                </Media.Body>
            </Media>
        );
    }

    capitalCase(value){
        return value.substring(0,1).toUpperCase() + value.substring(1)
    }


}

export default DogGridItem;