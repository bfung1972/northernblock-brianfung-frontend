import React,{Component} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { faStar as faStarOutline } from '@fortawesome/free-regular-svg-icons';
import './Favorited.css';

class Favorited extends Component {

  constructor(params){
    super(params);
    console.log("DOG " + this.props.dogName + " " + this.props.isFavorited)


    this.favoriteClicked = this.favoriteClicked.bind(this);
  }

  favoriteClicked(){
      /*this.setState({
        "isFavorited":!this.state.isFavorited
      });*/
     
      this.props.click(this.props.dogId,!this.props.isFavorited);
  }

  render() {
    return (

      <FontAwesomeIcon
        icon={this.props.isFavorited ? faStar : faStarOutline}
        className="Favorited__faIcon"
        fixedWidth={true}
        onClick={this.favoriteClicked}
      />

    );
  }
}


export default Favorited;
