import React,{Component} from 'react';
import DogGridItem from './DogGridItem';
import {Grid,Col,Row} from 'react-bootstrap';
import './DogGrid.css';


class DogGrid extends React.Component{
    
    constructor(props){
        super(props);
        this.compare = this.compare.bind(this);
        this.state = {
            "dogs": []
        }
        this.favoriteClicked = this.favoriteClicked.bind(this);
    }

    componentWillMount(){
        const url = "http://18.188.240.143:8080/dogs";
        fetch(url)
            .then((data) => data.json())
            .then((data) => {
                let dogs = [];
                data.map((dog) => dogs.push(dog) );
                dogs.sort(this.compare);
                this.setState({
                    "dogs":dogs
                });
                
            });
    }

    render(){
        return(<div>
                <Grid>
                    <Row className="show-grid">
                        {this.state.dogs.map((dog,index) => {
                            return(
                                <Col className="spaceBottom" xs={10} xsOffset={2} sm={6} smOffset={0} md={index%2 === 0 ? 5 : 6} mdOffset={index%2 === 0 ? 1 : 0}>
                                    <DogGridItem dog={dog} pos={index} favoriteClick={this.favoriteClicked} />
                                </Col>);
                        })}
                    </Row>
                </Grid>
        </div>);
    }

    compare(a,b) {
        if (a.favorite == true && b.favorite == false)
          return -1;          
        else
            if(a.favorite == false && b.favorite == true)
                return 1;
            else
                if(a.name < b.name)
                    return -1;
                else   
                    return 1;
      }

      favoriteClicked(dogId,isFavorite){
          //alert("favoriteClick" + dogId + " " + isFavorite);
          let dogs = this.state.dogs;
          let dog = dogs.find((item) => {
              return item.id === dogId;
          });
          console.log("dog name " + dog.name);
          dog.favorite = isFavorite;
          dogs.sort(this.compare);
          console.log(dogs);
          this.setState({
              "dogs":dogs
          })
      }
      
}

export default DogGrid;